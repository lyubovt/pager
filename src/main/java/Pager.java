import java.io.*;

/**
 * Created by timur on 14.09.17.
 */
public class Pager {
    public static void main(String[] args) {
        try (
                BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
                BufferedReader reader = new BufferedReader(new FileReader("text.txt"))
            ) {
            String input, line;
            while ((input = console.readLine()) != null) {
                for (int i = 0; i < 10; i++) {
                    line = reader.readLine();
                    if (line == null) {
                        System.exit(0);
                    }
                    System.out.println(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
